# TrackGen Repository

Welcome to the TrackGen Repository, my submission for the [Hackster.io contest](https://www.hackster.io/contests/SparkFun-NVIDIA-AI-Innovation-Challenge), proudly sponsored by [NVIDIA](https://www.nvidia.com/) and [SparkFun](https://www.sparkfun.com/). This project focuses on generating randomized race tracks for AI testing and other applications, leveraging generative AI technologies.

See the [project information](https://www.hackster.io/jesse-brockmann/track-generation-via-generative-ai-30c40b) at hackster.io

Watch a video of my simulator using the an generated map at [youtube](https://youtu.be/Le8VcR-OmXs)

## Features

TrackGen offers an innovative approach to generate new race tracks. These tracks can be utilized for a variety of purposes, including but not limited to:
- AI training and testing
- Simulation environments for autonomous vehicle research
- Game development

## Getting Started

For detailed instructions on how to use TrackGen, including setup, dependencies, and execution, please refer to the project's page on [hackster.io](https://www.hackster.io/jesse-brockmann/track-generation-via-generative-ai-30c40b).

## Licensing Information

This repository is subject to certain licensing conditions:

- The source code provided within this repository is available under specific licenses. Kindly review the license information in each file for more details.
- While the majority of this code is open for use, some portions have been adapted from third-party sources. I do not claim ownership of these components.
- The dataset, including training images, has its licensing terms. Please consult the `license.txt` file located in the data folder for comprehensive details.
- Contents within the `stylegan3` folder are licensed by Nvidia, adhering to their respective terms and conditions.

## Acknowledgements

Special thanks to:

- **Racelogic Ltd:** For allowing the use of CIR file data, which significantly contributed to the generation of the dataset for this project.
- **SparkFun and NVIDIA:** For organizing and sponsoring the contest, providing a platform for innovation and creativity.

## Support and Contact

Should you have any questions, feedback, or require assistance, please feel free to reach out to me:

- [Jesse Brockmann](https://www.linkedin.com/in/jesse-brockmann/)

Thank you for your interest in the TrackGen project. Your support and contributions are highly appreciated.
