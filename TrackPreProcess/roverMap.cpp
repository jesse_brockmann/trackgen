/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

/* This code is used to load a final map and generate map yaml file. Need to set the starting point and offset, and other yaml related items */

/* TODO: Add code to support multiple cars?  start1: x1,y1,heading1\nstart2: x2,y2,heading2\n ??? */

#define _CRT_SECURE_NO_WARNINGS

#include "roverMap.hpp"
#include "genericUtil.hpp"
#include "roverPost.hpp"

using namespace std;
using namespace genericUtil;
using namespace cv;

// Call back routine to handle mouse clicks and cursor movement
void onMouse(int event, int x, int y, int flags, void* userdata) {
   roverMap* map;
   map = reinterpret_cast<roverMap*>(userdata);

   if (event == EVENT_LBUTTONDOWN) {
      map->mouseEvent(x, y, 0);
      cout << "Mouse click at (" << x << ", " << y << ")" << endl;
   }
   else if (event == EVENT_RBUTTONDOWN) {
      map->mouseEvent(x, y, 1);
      cout << "Mouse click at (" << x << ", " << y << ")" << endl;
   }
   else if (event == EVENT_MOUSEMOVE) {
      map->mouseEvent(x, y, 2);
   }
}

roverMap::roverMap() {
   cnt = 0;
   haveDirection = false;
   haveOrigin = false;
}

// Write out a yaml with the values given
int roverMap::generateYAML(const char* mapName, const char* yamlFile, double scale, double offset_x, double offset_y, double heading) {
   char mapNameNoPath[1025];

   removePath(mapName, mapNameNoPath);

   FILE* fp = fopen(yamlFile, "w");
   if (!fp) {
      return(-1);
   }

   fprintf(fp, "image: %s\n", mapNameNoPath);
   fprintf(fp, "resolution: %.5lf\n", scale);
   fprintf(fp, "origin: [%lf, %lf, %lf]\n", offset_x, offset_y, 0.);
   fprintf(fp, "negate: 0\n");
   fprintf(fp, "occupied_thresh: 0.65\n");
   fprintf(fp, "free_thresh: 0.196\n");
   fprintf(fp, "start: [0.,0.,%lf]\n", heading);
   fclose(fp);
   return(0);
}

// Find the centerline of a map
int roverMap::findCenterLine() {
   vector<Vec4i> hierarchy;
   std::vector<std::vector<cv::Point>> centerContours;

   Mat inputImage;
   Mat thinImage;

   printf("Thresholding map\n");

   // Generated images may have non 255 values for track, so threshold the image first
   threshold(image, inputImage, 250, 256, 0);

   // Thin the track down to a skeleton
   printf("Thinning map\n");
   thinning(inputImage, thinImage);
   printf("Finding contours\n");

   // Find the contour of the skeleton using OpenCV
   findContours(thinImage, centerContours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, Point(0, 0));

   // Find the contour with the most points
   int i;
   int maxValue = 0;
   int maxNumberCenter = -1;
   for (i = 0; i < centerContours.size(); i++) {
      if (centerContours[i].size() > maxValue) {
         maxValue = static_cast<int>(centerContours.size());
         maxNumberCenter = i;
      }
   }

   // Working contour is the one with the most points
   // Remove bumps that are result artifacts from the thinning
   printf("Removing any bumps\n");
   centerLine = removeBumps(centerContours.at(maxNumberCenter), 20, false);

   // centerLine = centerContours.at(maxNumberCenter);
   return(0);
}

// Routine that handles mouse events
int roverMap::mouseEvent(int x, int y, int event) {
   // Start with fresh copy of image
   image.copyTo(image2);

   // Draw the cursor.  As some parts of the image are same color as the cursor this helps it standout
   if (event == 2) {
      line(image2, { x - 10,y }, { x + 10, y }, Scalar(50), 2);
      line(image2, { x,y - 10 }, { x, y + 10 }, Scalar(50), 2);
   }
   else if (event == 1) {
      // Right click, so reset
      cnt = 0;
      haveOrigin = false;
      haveDirection = false;
      origin.x = 0;
      origin.y = 0;
      direction.x = 0;
      direction.y = 0;
   }
   else {
      // First left click, set origin
      if (cnt == 0) {
         if (!haveOrigin) {
            haveOrigin = true;
            origin.x = x;
            origin.y = y;
            cnt = 1;
           
         }
      }
      else if (cnt == 1) { // Second left click, set direction
         if (!haveDirection) {
            direction.x = x;
            direction.y = y;
            haveDirection = true;
            cnt = 0;
           
         }
      }
   }

   // Display text based on current state
   if (!haveOrigin) {
      putText(image2, "Left click location to set starting point", textPt, FONT_HERSHEY_PLAIN, 1.0, Scalar(255));
   }
   else if (haveOrigin && !haveDirection) {
      putText(image2, "Left click location to set direction on map", textPt, FONT_HERSHEY_PLAIN, 1.0, Scalar(255));
   }
   else {
      putText(image2, "Hit space to quit and save map yaml or right click to reset", textPt, FONT_HERSHEY_PLAIN, 1.0, Scalar(255));
   }

   // Draw the origin if it's set
   if (haveOrigin) {
      circle(image2, origin, 5, Scalar(20), FILLED);
   }

   // Draw a line for the direction if it's set
   if (haveDirection) {
      line(image2, origin, direction, Scalar(20), 2);
   }

   // Update the image
   imshow("Map", image2);
   return(0);
}

int roverMap::findOrigin(const char* name, double scale, double& offset_x, double& offset_y, double& heading) {
   offset_x = 0;
   offset_y = 0;
   heading = 0;

   // Read the image, should be grayscale
   image = imread(name, IMREAD_GRAYSCALE);
   if (image.empty()) {
      return(-1);
   }

   // Images are shown by nav2 rotated 90 degrees, but this might be confusing to the user??
   // Default option is to show images asis, but the following line could change that.
   // HOWEVER X/Y would have to be adapted for this change, and it's not currently being done
   // rotate(image, image, cv::ROTATE_90_COUNTERCLOCKWISE);

   // Create a OpenCV window
   namedWindow("Map", WINDOW_AUTOSIZE);
   image.copyTo(image2);

   // Show the map
   imshow("Map", image2);
   // Just to refresh/draw the image
   waitKey(50);

   // Set the callback function for mouse events
   setMouseCallback("Map", onMouse, this);

   // Find the centerline of the track
   findCenterLine();

   int i;
   // Draw the centerline of the track... on the primary image
   for (i = 0; i < centerLine.size(); i++) {
      Point pt1 = { centerLine[i].x, centerLine[i].y };
      Point pt2;
      if (i == centerLine.size() - 1) {
         pt2 = { centerLine[0].x, centerLine[0].y };
      }
      else {
         pt2 = { centerLine[i + 1].x, centerLine[i + 1].y };
      }

      line(image, pt1, pt2, Scalar(128), 2, LINE_8);
   }

   // Display the image
   image.copyTo(image2);

   // Display text instructions
   putText(image2, "Left click location to set starting point", textPt, FONT_HERSHEY_PLAIN, 1.0, Scalar(255));
   // Redraw the map
   imshow("Map", image2);

   bool done = false;
   // Wait for key input, mouse event or the window is closed
   do {
      // This will detect if the window was closed
      if (getWindowProperty("Map", cv::WND_PROP_VISIBLE) < 1) {
         printf("Window was closed\n");
         return(-1);
      }

      char c = static_cast<char>(waitKey(0));
      // Space was hit, so done processing
      if (c == ' ') {
         done = true;
      }
   } while (!done);

   if (haveOrigin) {
      // Set the correct offsets if origin was set
      offset_x = -origin.x * scale;
      offset_y = (image.rows-origin.y) * -scale;

      // Set the correct heading if origin and direction point were given
      if (haveDirection) {
         heading = find_heading(direction.x, direction.y, origin.x, origin.y);
      }
   }
   return(0);
}

int roverMap::load(int argc, char** argv, bool verbose, double scale) {
   if (argc < 3) {
      printf("Number of args given %d < 3", argc);
      return(-1);
   }
   printf("Generating map files\n");

   int i;
   for (i = 2; i < argc; i++) {
      cnt = 0;
      string name = argv[i];
      char outputName[1025];
      char fileNameNoPath[1025];
      char fileNameNoPath2[1025];
      char fileName[1025];
      char path[1025];

      // Find just the path
      removeFileName(name.c_str(), path);

      // Remove file extension from file name
      removeFileExtension(name.c_str(), fileName);

      // Does file have a path?
      bool havePath = removePath(name.c_str(), fileNameNoPath);

      // Remove the file extension
      removeFileExtension(fileNameNoPath, fileNameNoPath2);

      // If have path, put the yaml in the same folder
      if (havePath) {
#ifdef _LINUX
         snprintf(outputName, 1024, "%s/%s.yaml", path, fileNameNoPath2);
#else
         snprintf(outputName, 1024, "%s\\%s.yaml", path, fileNameNoPath2);
#endif
      }
      else {
         // Else just write yaml to current folder
         snprintf(outputName, 1024, "%s.yaml", fileNameNoPath2);
      }


      printf("Input %s Output %s\n", name.c_str(), outputName);

      // Make sure the image file exists (could do a check it's an png file, but I believe the user will do the right thing?)
      FILE* fp;
      fp = fopen(name.c_str(), "r");
      if (!fp) {
         fprintf(stderr, "ERROR image(map) file named %s does not exist\n", name.c_str());
      }
      else {
         fclose(fp);

         double offset_x, offset_y, heading;

         // Prompt the user to set the origin and heading of the car in the map.
         int rval = findOrigin(name.c_str(), scale, offset_x, offset_y, heading);
         if (rval == 0) {
            // Adding correct offset for heading.  (Remember the rotation in found origin? Not sure that is why)
            heading += PI / 2;
            printf("Writing yaml file %s\n", outputName);
            generateYAML(name.c_str(), outputName, scale, offset_x, offset_y, heading);
         }
         else {
            fprintf(stderr,"Window was closed instead of space being hit. Will not write out yaml file\n");
         }
      }
   }


   return(0);
};
