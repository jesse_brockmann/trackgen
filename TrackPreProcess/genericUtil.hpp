/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */
#ifndef GENERIC_UTIL_HPP
#define GENERIC_UTIL_HPP

#include <cstdint>
#include <cstdio>
#include <string>
#include <vector>

#define GRAVITY 9.81

namespace genericUtil {
   class point2D {
   public:
      point2D() {
         x = 0.;
         y = 0.;
      }

      point2D(double xIn, double yIn) {
         x = xIn;
         y = yIn;
      }
      double x, y;
   };

   class point2Df {
   public:
      point2Df() {
         x = 0.;
         y = 0.;
      }

      point2Df(float xIn, float yIn) {
         x = xIn;
         y = yIn;
      }
      float x, y;
   };

   class point2Di {
   public:
      point2Di() {
         x = 0;
         y = 0;
      }

      point2Di(int xIn, int yIn) {
         x = xIn;
         y = yIn;
      }
      int x, y;
   };

   /*! Convert a given angle from radians to degrees */
   double toDegree(double r);
   /*! Convert a given angle from degrees to radians */
   double toRadians(double d);
   /* Remove all instances of character c from a string. Returns the number found */
   int removeChar(char* str, char c);
   /* Remove all instances of character c from a string. Returns the number found */
   std::string removeChar(std::string str, char charToRemove);
   // This routine has not been tested yet
   std::vector<std::string> splitString(char* in, const char* sep);

   // Remove just the file name from a string, leave the path
   bool removeFileName(const char* filename, char* out);
   // Remove the path from a string, leaving only the filename
   bool removePath(const char* filename, char* out);
   // Remove path and filename, but return the extension of a string
   bool getFileExtension(const char* filename, char* out);
   // Remove just the extension from a string
   bool removeFileExtension(const char* filename, char* out);
   // Look through a string for items separated by "sep" and return the number found
   // And set ptrs to each of these found substrings. Only allow a maximum of "max" number of sub strings
   // if max is reached will return number found so far
   int splitString(char* in, const char* sep, char* ptrs[], int max);
   // For a given angle in degrees make sure the range is between 0 and 360
   double wrap360(double angle);
   // For a given angle in degrees make sure the range is between -180 and 180 degrees
   double wrap180(double angle);
   // For a given angle in radians make sure the range is between -PI and PI
   double wrapRadians(double angle);
   // For a given angle in radians make sure the range is between 0 and 2*PI
   double wrapRadians2PI(double angle);
   // Returns the angle difference between two angles in degrees and keeps between range of -180 to 180 degrees
   double angleDifference(double a1, double a2);
   // Returns the angle required to turn by a given angle given two angles??
   double veering(double heading, double desired);
   // Limit a value between min and max.  Will set to min if value is less then min, or to max if beyond max
   int limitRange(int value, int min, int max);
   // Limit a value between min and max.Will set to min if value is less then min, or to max if beyond max
   double limitRange(double value, double min, double max);

   // Return the minimum of two values
   inline double minValue(double v1, double v2) {
      if (v1 > v2) {
         return(v2);
      }
      return(v1);
   }

   // Return the minimum of two values
   inline int minValue(int v1, int v2) {
      if (v1 > v2) {
         return(v2);
      }
      return(v1);
   }

   // Return the maximum of two values
   inline double maxValue(double v1, double v2) {
      if (v1 > v2) {
         return(v1);
      }
      return(v2);
   }

   // Return the maximum of two values
   inline int maxValue(int v1, int v2) {
      if (v1 > v2) {
         return(v1);
      }
      return(v2);
   }

   std::vector<std::string> FindFilesWithExtension(const char* directory, const char* ext);

   // Returns the rate of an item in seconds.  So pass in 2 result is 0.5, meaning for a rate of two times a second need to evaluate every 0.5 seconds.
   double perSecond(double in);
   // Remove all \n and \r and space characters from the end of a line
   void removeEOLChars(char* in);
   // Convert a string to uppercase
   void makeUppercase(char* in);
   // Convert a string to uppercase
   std::string makeUppercase(const std::string& str);
   // Convert a string to lowercase
   void makeLowercase(char* in);
   // Convert a string to lowercase
   std::string makeLowercase(const std::string& str);
   // Convert a value from meters to feet
   double MeterToFoot(double in);
   // Convert a value from Feet to Meters
   double FootToMeter(double in);
   // Find distance between two points
   double distance(double x1, double y1, double x2, double y2);
   // Find distance between two points
   double distance(point2D p1, point2D p2);
   // Find distance between two points
   float distance(point2Df p1, point2Df p2);
   // determine the position of a point  P relative to a line defined by two points
   // If u is between 0 and 1 it means the point is on the line segment
   // If u is less then 0 then 0 then it's to the one side or > 1 on the other side
   double unit_calc(double L1x, double L1y, double L2x, double L2y, double Px, double Py);

   // Calculates to point of intersection or the nearest point of a line defined by two points L1 and L2, and point P
   // And returns the result in Ix and Iy
   void intercept(double L1x, double L1y, double L2x, double L2y, double Px, double Py, double& Ix, double& Iy);
   // Calculates the slope of a line.  If 
   double findSlope(double x1a, double y1a, double x1b, double y1b, bool& vertical);
   float findSlope(float x1a, float y1a, float x1b, float y1b, bool& vertical);
   double findSlope(const point2D& p1, const point2D& p2, bool& vertical);
   float findSlope(const point2Df& p1, const point2Df& p2, bool& vertical);
   // Finds the direction of travel from two points
   double findHeading(double x1a, double y1a, double x1b, double y1b);
   // Finds the direction of travel from two points
   double findHeading(point2D p1, point2D p2);
   // Finds the direction of travel from two points
   float findHeading(point2Df p1, point2Df p2);

   // Rotate a point relative to origin of 0,0 by theta radians
   void rotateAngle(double thetaRadians, double x_in, double y_in, double& x_out, double& y_out);

   // Rotate a point relative to an origin by theta radians
   void rotateAroundOrigin(double theta, double x_in, double y_in, double x_origin, double y_origin, double& x_out, double& y_out);


   double pointToSegmentDistance(double px, double py,
      /*                      */ double ax, double ay,
      /*                      */ double bx, double by);

   point2D findPointAtDistance(const point2D& p1, const point2D& p2, double d);

   double distancePointToLine(double ax, double ay,
      /*                   */ double bx, double by,
      /*                   */ double cx, double cy);

   bool nearlyEqual(double a, double b);

   int findSide(double ax, double ay,
      /*     */ double bx, double by,
      /*     */ double cx, double cy);

   double calculateCrossTrackError(double x, double y,
      /*                        */ double x1, double y1,
      /*                        */ double x2, double y2);

   /*Scan in a line of txt from a file */
   int fscanLine(FILE* fp, char* data, int len, bool& done);

   void minSecToStr(char* str, int min, int sec);
   void msToMinSec(int et, int& min, int& sec);

   void minSecMsToStr(char* str, int min, int sec, int msec);
   void msToMinSecMs(int et, int& min, int& sec, int& msec);

   void crc_accumulate(uint8_t data, uint16_t& crcAccum);
   uint16_t crc_calculate(uint8_t* pBuffer, int length);


#define HALF_PI 1.57079632679
#define PI 3.14159265358979323846
#define TWO_PI 6.28318530718
#define QTR_PI 0.785398163395

   int per_sec_to_ms(int in);
   void wdt_feed();
   void wDelay(unsigned long amt);
   double degree2rad(double degree);
   double rad2degree(double rad);
   float degree2rad(float degree);
   float rad2degree(float rad);
   double MtoFT(double in);
   double FTtoM(double in);
   double InchToM(double in);
   double MToInch(double in);
   void uppercase(char* in);
   void getDateTimeString(char* buffer, int bufferSz);
   double find_heading(double x1a, double y1a, double x1b, double y1b);
   double find_slope(double x1a, double y1a, double x1b, double y1b);
   void rotate_angle(double theta, double x_in, double y_in, double* x_out, double* y_out);

   // Given two lines find the angle different between them and return the normalized angle in radians
   // The sign of the result indicates the direction of rotation.  positive angle means to rotate the first line towards the second a clockwise rotation
   // is required and a negative angle means counterclockwise angle is required.
   double find_angle_2lines(double x1a, double y1a, double x1b, double y1b, double x2a, double y2a, double x2b, double y2b);

   void msSleep(uint32_t ms);
   // Convert HSV (Hue (0-360) Saturation (0-1) Value (0-1)) values to RGB (0-255)
   void HSVToRGB(double h, double s, double v, uint8_t& R, uint8_t& G, uint8_t& B);

   void calculateMidpoint(double x1, double y1, double x2, double y2, double& xOut, double& yOut);
   double calculateSlope(double x1, double y1, double x2, double y2);
   /* Given two points (a line segment) find a line perpendicular to that line.  The outputed line with be at the center point of the line,
    * and that line will be the same size of the input line */
   void perpendicularLine(double x1, double y1, double x2, double y2, double dist, double& xMid, double& yMid, double& xOut1, double& yOut1, double& xOut2, double& yOut2);

   int valueRange(int min, int value, int max);
   double valueRange(double min, double value, double max);


   bool calculateLine(double x1, double y1, double x2, double y2, double& slope, double& yIntercept);
   bool findIntersectionGivenSlope(double m1, double c1, double m2, double c2, double& x, double& y);
   bool findLineIntersection(point2D p1a, point2D p1b, point2D p2a, point2D p2b, double& x, double& y);
   point2D extendLine(point2D A, point2D B, point2D C, double distance);
}

#endif

