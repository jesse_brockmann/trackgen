/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

#ifndef ROVER_CIRFILE_HPP
#define ROVER_CIRFILE_HPP
#include <vector>
#include <opencv2/opencv.hpp>

class  roverCIRFilePoint {
public:
   roverCIRFilePoint() {
      rawLat = 0;
      rawLon = 0;
      lat = 0;
      lon = 0;
      z = 0.;
      mapX = 0;
      mapY = 0;
   }
   double rawLat;
   double rawLon;
   double lat;
   double lon;
   double z;
   double mapX;
   double mapY;
};

class roverCIRFile {
public:
   roverCIRFile() {

   }
   int convertAll(int argc, char* argv[], bool augment, bool verbose);
   int loadCIRFile(const char* filename);
   cv::Mat convertToImage(int sizeX, int sizeY, double& scaleIO, double& rangeX, double& rangeY, bool forceScale, bool& err, bool verbose);
private:
   cv::Scalar black = cv::Scalar(0, 0, 0);
   cv::Scalar white = cv::Scalar(255, 255, 255);
   cv::Scalar red = cv::Scalar(0, 0, 255);
   cv::Scalar green = cv::Scalar(0, 255, 0);
   cv::Scalar blue = cv::Scalar(255, 0, 0);
   cv::Scalar yellow = cv::Scalar(0, 255, 255);
   int findMapContours();
   int processMap();
   cv::Mat image;
   cv::Mat thresholdMap;
   cv::Mat mapWithWalls;
   cv::Mat mapWithCenterLine;
   std::vector<cv::Point> outerContour;
   std::vector<cv::Point> innerContour;
   std::vector<cv::Point> centerContour;

   roverCIRFilePoint basePoint;
   bool convertRawLatLon(double rawLat, double rawLon, double& lat, double& lon);
   std::vector<roverCIRFilePoint> points;
};
#endif
