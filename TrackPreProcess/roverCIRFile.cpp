/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

 /* This code is designed to take an CIR file and create a raw image for use in training, or even racing with processing */

#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <cmath>
#include "roverCIRFile.hpp"
#include "roverUtility.hpp"
#include "genericUtil.hpp"
using namespace genericUtil;
using namespace std;
using namespace cv;

Mat roverCIRFile::convertToImage(int sizeX, int sizeY, double& scaleIO, double& rangeX, double& rangeY, bool forceScale, bool& err, bool verbose) {
   Mat outputImage = Mat::zeros(sizeX, sizeY, CV_8UC1);
   size_t i;
   err = false;

   // For each point find the heading/distance between the base point
   for (i = 1; i < points.size(); i++) {
      double distance = distanceBetween(basePoint.lat, basePoint.lon, points[i].lat, points[i].lon);
      double heading = courseTo(basePoint.lat, basePoint.lon, points[i].lat, points[i].lon);

      // Project these to x/y using the distance and heading
      points[i].mapX = cos(heading) * distance;
      points[i].mapY = sin(heading) * distance;
   }

   // Find the min/max of x and y
   double minX, minY;
   double maxX, maxY;
   minX = 99999999;
   minY = 99999999;
   maxX = -99999999;
   maxY = -99999999;

   for (i = 0; i < points.size(); i++) {
      if (points[i].mapX > maxX) {
         maxX = points[i].mapX;
      }
      if (minX > points[i].mapX) {
         minX = points[i].mapX;
      }

      if (points[i].mapY > maxY) {
         maxY = points[i].mapY;
      }
      if (minY > points[i].mapY) {
         minY = points[i].mapY;
      }
   }

   // Find the range of x and y
   rangeX = maxX - minX;
   rangeY = maxY - minY;
   if (verbose) {
      printf("Size X %lf\n", rangeX);
      printf("Size Y %lf\n", rangeY);
   }

   // Want the coordinate with the maximum range
   double maxRange;
   if (rangeX > rangeY) {
      maxRange = rangeX;
   }
   else {
      maxRange = rangeY;
   }

   // Calculate the scale of this image
   double calculatedScale = maxRange / static_cast<double>(sizeX - 2);
   calculatedScale = 1. / calculatedScale;

   /* If scale is less then 0.7 or greater then 2.0 skip this image.
    * These numbers were determined by finding the scale of all images
    * And finding a nice middle ground to eliminate as the small number
    * of maps as possible
    */
   if (calculatedScale < 0.7 || calculatedScale > 2.) {
      err = true;
      return(outputImage);
   }

   double scale;

   // Once every map has been processed need to find a common scale for all of the maps
   if (!forceScale) {
      scale = calculatedScale;
      scaleIO = scale;
   }
   else {
      scale = scaleIO;
   }

   int offsetX = (sizeX - static_cast<int>(rangeX * scale)) / 2;
   int offsetY = (sizeY - static_cast<int>(rangeY * scale)) / 2;

   if (verbose) {
      printf("Scale %lf\n", scale);
   }

   // Draw all the points on the image
   for (i = 1; i < points.size(); i++) {
      Point pt1 = { static_cast<int>((points[i - 1].mapX - minX) * scale), static_cast<int>((points[i - 1].mapY - minY) * scale) };
      Point pt2 = { static_cast<int>((points[i].mapX - minX) * scale), static_cast<int>((points[i].mapY - minY) * scale) };

      pt1.x += offsetX;
      pt1.y += offsetY;
      pt2.x += offsetX;
      pt2.y += offsetY;

      if (verbose) {
         printf("PT1 %d %d to %d %d\n", pt1.x, pt1.y, pt2.x, pt2.y);
      }
      // Draw the lines in white
      line(outputImage, pt1, pt2, cv::Scalar(255, 255, 255), 1);
   }
   return(outputImage);
}

bool roverCIRFile::convertRawLatLon(const double rawLat, const double rawLon, double& lat, double& lon) {
   double tmpLat, tmpLon;

   // Divide the minutes by 60 to get degrees
   tmpLat = rawLat / 60.;
   tmpLon = rawLon / 60.;

   lat = tmpLat;
   lon = tmpLon;
   return(true);
}

int roverCIRFile::findMapContours() {
   int i;
   Mat canny_output;
   vector<Vec4i> hierarchy;
   vector<Vec4i> hierarchy2;
   std::vector<std::vector<cv::Point>> outerContours;
   std::vector<std::vector<cv::Point>> innerContours;


   // See https://docs.opencv.org/2.4/doc/tutorials/imgproc/shapedescriptors/find_contours/find_contours.html for more information

   /// Find contours
   findContours(thresholdMap, outerContours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, Point(0, 0));
   outerContour = outerContours.at(0);

   Mat thresholdMap2;
   thresholdMap.copyTo(thresholdMap2);

   // Wipe out the outer lines to find the inner lines
   for (i = 0; i < outerContours.size(); i++) {
      drawContours(thresholdMap2, outerContours, i, Scalar(0), 2, LINE_8, hierarchy, 0, Point()); // This will wipe out the outsize lines on the map
   }

   // Find the contours again (inner ones)
   mapWithWalls = Mat::zeros(1024, 1024, CV_8UC1);
   findContours(thresholdMap2, innerContours, hierarchy2, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, Point(0, 0));

   // For the inner and outer contours find the contour of the maximum size
   int maxValue = 0;
   int maxNumberInner = -1;
   for (i = 0; i < innerContours.size(); i++) {
      if (innerContours[i].size() > maxValue) {
         maxValue = static_cast<int>(innerContours.size());
         maxNumberInner = i;
      }
   }

   if (maxNumberInner >= 0) {
      innerContour = innerContours.at(maxNumberInner);
      for (i = 0; i < innerContour.size(); i++) {
         circle(mapWithWalls, innerContour.at(i), 1, white, -1);
      }
   }

   maxValue = 0;
   int maxNumberOuter = -1;
   for (i = 0; i < outerContours.size(); i++) {
      if (outerContours[i].size() > maxValue) {
         maxValue = static_cast<int>(outerContours.size());
         maxNumberOuter = i;
      }
   }

   // Make have a outer line that's reasonable
   if (maxNumberOuter >= 0) {
      outerContour = outerContours[maxNumberOuter];

      // Outline the outer track with a circle of radius one
      for (i = 0; i < outerContour.size(); i++) {
         circle(mapWithWalls, outerContour.at(i), 1, white, -1);
      }

      int j;

      // For each outer point find the closest inner point
      for (i = 0; i < outerContour.size(); i++) {
         double minDist = 9999999;
         int minItem = -1;
         // Find the closest inner point
         for (j = 0; j < innerContour.size(); j++) {
            double dist = sqrt(pow(innerContour.at(j).x - outerContour[i].x, 2) + pow(innerContour.at(j).y - outerContour[i].y, 2));
            if (dist < minDist) {
               minDist = dist;
               minItem = j;
            }
         }

         if (minItem >= 0) {
            // With a width of two, draw from the outer point to the closest inner point
            line(mapWithWalls, outerContour[i], innerContour[minItem], white, 2);

            // Draw from previous outer point, to the closest inner contour point
            if (i > 0) {
               line(mapWithWalls, outerContour[i - 1], innerContour[minItem], white, 2);
            }

            // Draw from the next outer point, to the closest inner contour pint
            if (i < outerContour.size() - 1) {
               line(mapWithWalls, outerContour[i + 1], innerContour[minItem], white, 2);
            }
         }
      }

      // For each inner point find the closest outer point
      for (i = 0; i < innerContour.size(); i++) {
         double minDist = 9999999;
         int minItem = -1;

         // Find the closest outer point
         for (j = 0; j < outerContour.size(); j++) {
            double dist = sqrt(pow(innerContour.at(i).x - outerContour[j].x, 2) + pow(innerContour.at(i).y - outerContour[j].y, 2));
            if (dist < minDist) {
               minDist = dist;
               minItem = j;
            }
         }

         if (minItem >= 0) {
            // With a width of two, draw from the closet outer point to the inner point
            line(mapWithWalls, outerContour[minItem], innerContour[i], white, 2);

            // With a width of two, draw from the closet outer point to the previous inner point
            if (i > 0) {
               line(mapWithWalls, outerContour[minItem], innerContour[i - 1], white, 2);
            }

            // With a width of two, draw from the closet outer point to the next inner point
            if (i + 1 < innerContour.size()) {
               line(mapWithWalls, outerContour[minItem], innerContour[i + 1], white, 2);
            }
         }
      }
   }

   mapWithWalls.copyTo(mapWithCenterLine);
   return(0);
}

int roverCIRFile::processMap() {
   Mat thresholdMap2;

   image.copyTo(thresholdMap);
   findMapContours();
   return(0);
}

int roverCIRFile::convertAll(int argc, char* argv[], bool augment, bool verbose) {
   printf("Converting cir files\n");
   int i;
   bool forceScale = true;
   if (argc < 3) {
      printf("No directory given\n");
      return(-1);
   }

   if (verbose) {
      printf("Scanning folder %s for .CIR files\n", argv[2]);
   }

   std::vector<double> mapScale;

   // Find all files with CIR extension from the folder provided
   std::vector<string> files = FindFilesWithExtension(argv[2], ".cir");
   for (i = 0; i < files.size(); i++) {
      char path[1025];
      char outputName[1025];
      char fileNameNoPath[1025];
      char fileNameNoPath2[1025];
      char dataName[1025];
      char fileName[1025];
      char wallName[1025];

      // Use the name of the CIR file to find the name of the outputted image and dat files
      // Remove the file extension
      removeFileExtension(files[i].c_str(), fileName);
      // Remove the filename to get just the path
      removeFileName(files[i].c_str(), path);
      // Remove the path to get JUST the filename
      removePath(files[i].c_str(), fileNameNoPath);
      // Remove the extension from just the filename
      removeFileExtension(fileNameNoPath, fileNameNoPath2);

      // Simple output is image with just the outline of the track in the same folder as the .cir file with extension png
      snprintf(outputName, 1024, "%s.png", fileName);

      // Dat (data) output in the same folder as the .cir file with extension dat
      snprintf(dataName, 1024, "%s.dat", fileName);

      // Final image output in the same folder as the .cir file, but adding a subdirectory named wall.  But the same name as the cir file with png extension
#ifdef _LINUX
      snprintf(wallName, 1024, "%s/wall/%s_0.png", path, fileNameNoPath2);
#else 
      snprintf(wallName, 1024, "%s\\wall\\%s_0.png", path, fileNameNoPath2);
#endif

      printf("Input %s Output %s Data %s\n", files[i].c_str(), outputName, dataName);

      // Reset for this particular cir file
      points.clear();
      loadCIRFile(files[i].c_str());
      double scale, rangeX, rangeY;
      scale = 0.7; // This is hard coded based on testing of maps
      bool err;

      // Convert these coordinates into lined image
      image = convertToImage(1024, 1024, scale, rangeX, rangeY, forceScale, err, verbose);

      if (!err) {
         // Write out the lined map
         imwrite(outputName, image);

         // Take the outline map, and fill it in
         processMap();

         // Write out the base image
         imwrite(wallName, mapWithWalls);

         // Augment the maps by rotating x, y and x and y
         if (augment) {
            Mat image2;
            cv::flip(mapWithWalls, image2, 0);
#ifdef _LINUX
            snprintf(outputName, 1024, "%s/wall/%s_A.png", path, fileNameNoPath2);
#else
            snprintf(outputName, 1024, "%s\\wall\\%s_A.png", path, fileNameNoPath2);
#endif
            imwrite(outputName, image2);

            cv::flip(mapWithWalls, image2, 1);
#ifdef _LINUX
            snprintf(outputName, 1024, "%s/wall/%s_B.png", path, fileNameNoPath2);
#else
            snprintf(outputName, 1024, "%s\\wall\\%s_B.png", path, fileNameNoPath2);
#endif
            imwrite(outputName, image2);

            cv::flip(mapWithWalls, image2, -1);
#ifdef _LINUX
            snprintf(outputName, 1024, "%s/wall/%s_C.png", path, fileNameNoPath2);
#else
            snprintf(outputName, 1024, "%s\\wall\\%s_C.png", path, fileNameNoPath2);
#endif
            imwrite(outputName, image2);
         }

         // Add to the scale list if it isn't hard coded
         if (!forceScale) {
            mapScale.emplace_back(scale);
         }

         // For each map write out a file with data on the map
         FILE* fp = fopen(dataName, "w");
         if (fp) {
            fprintf(fp, "%lf %lf %lf\n", scale, rangeX, rangeY);
            fclose(fp);
         }
      }
   }


   if (!forceScale) {
      // Sort the maps to see what the best scale for all the images would be
      std::sort(mapScale.begin(), mapScale.end());
      if (verbose) {
         for (i = 0; i < mapScale.size(); i++) {
            printf("%lf\n", mapScale[i]);
         }
         printf("%zd\n", mapScale.size());
      }
   }
   return(0);
}

int roverCIRFile::loadCIRFile(const char* filename) {
   FILE* fp;

   /* Read a cir file */
   fp = fopen(filename, "r");
   if (!fp) {
      return(-1);
   }

   // This will store the column names of the data in the cir file
   vector<string> columnNames;
   // File still has more data
   bool done = false;
   // In the data section of the file?
   bool inData = false;
   // Pointers for parsing lines
   char* ptrs[51];
   // What section of the file currently reading
   int section = 0;
   // Single line of text from the cir file
   char line[1025];
   // Lat/Lon values
   double lat, lon;
   // Raw Lat/Lon values
   double rawLat, rawLon;
   // A data point in the CIR file
   roverCIRFilePoint point;

   // Lat/Lon columns in the data.  -1 = not set
   int latColumn = -1;
   int lonColumn = -1;

   // Read the entire file
   while (!done) {
      // Read single line
      int rval = fscanLine(fp, line, 1024, done);
      // If the read was successful parse the line
      if (rval > 0) {
         // Remove extra characters at the end of a line
         removeEOLChars(line);

         // If inData == true then currently reading data section from the file
         if (inData) {
            // Data is separated by spaces
            int cnt = splitString(line, " ", ptrs, 50);

            // Need at least two values for lat/lon
            if (cnt >= 2) {
               // Must know where lat/lon values are in data to read them
               if ((latColumn >= 0) && (lonColumn >= 0)) {
                  // Convert strings to doubles
                  rawLat = strtod(ptrs[latColumn], nullptr);
                  rawLon = strtod(ptrs[lonColumn], nullptr);
                  // Convert from degree seconds to degrees
                  convertRawLatLon(rawLat, rawLon, lat, lon);
                  // Save this point for later
                  point.rawLat = rawLat;
                  point.rawLon = rawLon;
                  point.lat = lat;
                  point.lon = lon;
                  points.emplace_back(point);
               }
            }
         }
         else {
            // Look for the various sections in the cir file
            if (strcmp(line, "[data]") == 0) {
               inData = true;
            }
            else if (strcmp(line, "[header]") == 0) {
               section = 1;
            }
            else if (strcmp(line, "[comments]") == 0) {
               section = 2;
            }
            else if (strcmp(line, "[column names]") == 0) {
               section = 3;
            }
            else {
               // Look at the columns to find what values in the data our the lat/long needed
               if (section == 3) {
                  int cnt = splitString(line, " ", ptrs, 50);
                  if (cnt >= 2) {
                     int i;
                     // Find the lat/lon labels
                     for (i = 0; i < cnt; i++) {
                        if (strcmp(ptrs[i], "lat") == 0) {
                           latColumn = i;
                        }
                        else if (strcmp(ptrs[i], "long") == 0) {
                           lonColumn = i;
                        }
                     }
                  }
                  // Didn't find the lat or lon? :(
                  if ((latColumn == -1) || (lonColumn == -1)) {
                     fprintf(stderr, "Didn't find lat (%d) or long (%d) in the column data!\n", latColumn, lonColumn);
                  }
               }
            }
         }
      }
   }

   // Points were found.  The first point is the "base point"
   if (!points.empty()) {
      basePoint = points[0];
   }
   fclose(fp);
   return(0);
}
