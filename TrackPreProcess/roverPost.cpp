/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

/* This code is designed to take an raw image, and convert it into a final track */

#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <cmath>
#include "roverPost.hpp"
#include "roverUtility.hpp"
#include "genericUtil.hpp"
using namespace genericUtil;
using namespace std;
using namespace cv;

/**
 * Code for thinning a binary image using Zhang-Suen algorithm.
 *
 * Author:  Nash (nash [at] opencv-code [dot] com)
 * Website: http://opencv-code.com
 *
 * Perform one thinning iteration.
 * Normally you wouldn't call this function directly from your code.
 *
 * Parameters:
 * 		im    Binary image with range = [0,1]
 * 		iter  0=even, 1=odd
 */

void thinningIteration(cv::Mat& img, int iter) {
   CV_Assert(img.channels() == 1);
   CV_Assert(img.depth() != sizeof(uchar));
   CV_Assert(img.rows > 3 && img.cols > 3);

   cv::Mat marker = cv::Mat::zeros(img.size(), CV_8UC1);

   int nRows = img.rows;
   int nCols = img.cols;

   if (img.isContinuous()) {
      nCols *= nRows;
      nRows = 1;
   }

   int x, y;
   uchar* pAbove;
   uchar* pCurr;
   uchar* pBelow;
   uchar* nw, * no, * ne;    // north (pAbove)
   uchar* we, * me, * ea;
   uchar* sw, * so, * se;    // south (pBelow)

   uchar* pDst;

   // initialize row pointers
   pAbove = NULL;
   pCurr = img.ptr<uchar>(0);
   pBelow = img.ptr<uchar>(1);

   for (y = 1; y < img.rows - 1; ++y) {
      // shift the rows up by one
      pAbove = pCurr;
      pCurr = pBelow;
      pBelow = img.ptr<uchar>(y + 1);

      pDst = marker.ptr<uchar>(y);

      // initialize col pointers
      no = &(pAbove[0]);
      ne = &(pAbove[1]);
      me = &(pCurr[0]);
      ea = &(pCurr[1]);
      so = &(pBelow[0]);
      se = &(pBelow[1]);

      for (x = 1; x < img.cols - 1; ++x) {
         // shift col pointers left by one (scan left to right)
         nw = no;
         no = ne;
         ne = &(pAbove[x + 1]);
         we = me;
         me = ea;
         ea = &(pCurr[x + 1]);
         sw = so;
         so = se;
         se = &(pBelow[x + 1]);

         int A = (*no == 0 && *ne == 1) + (*ne == 0 && *ea == 1) +
            (*ea == 0 && *se == 1) + (*se == 0 && *so == 1) +
            (*so == 0 && *sw == 1) + (*sw == 0 && *we == 1) +
            (*we == 0 && *nw == 1) + (*nw == 0 && *no == 1);
         int B = *no + *ne + *ea + *se + *so + *sw + *we + *nw;
         int m1 = iter == 0 ? (*no * *ea * *so) : (*no * *ea * *we);
         int m2 = iter == 0 ? (*ea * *so * *we) : (*no * *so * *we);

         if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
            pDst[x] = 1;
      }
   }

   img &= ~marker;
}

/**
 * Code for thinning a binary image using Zhang-Suen algorithm.
 *
 * Author:  Nash (nash [at] opencv-code [dot] com)
 * Website: http://opencv-code.com
 *
 * Function for thinning the given binary image
 *
 * Parameters:
 * 		src  The source image, binary with range = [0,255]
 * 		dst  The destination image
 */
void thinning(const cv::Mat& src, cv::Mat& dst)
{
   dst = src.clone();
   dst /= 255;         // convert to binary image

   cv::Mat prev = cv::Mat::zeros(dst.size(), CV_8UC1);
   cv::Mat diff;

   do {
      thinningIteration(dst, 0);
      thinningIteration(dst, 1);
      cv::absdiff(dst, prev, diff);
      dst.copyTo(prev);
   } while (cv::countNonZero(diff) > 0);

   dst *= 255;
}

// Some images can end up with "bumps" when the image gets thicker/or thinner at points and this
// Will try to find these bumps and remove them
// Bumps generally are not part of the main profile.. they loop around back to the profile, so seeking
// points that get further away and then closer to a previous point.  If they get extremely close they
// are a bump and that bump is removed in the final profile
std::vector<cv::Point> removeBumps(std::vector<cv::Point> contourIn, int numPoints, bool verbose) {
   std::vector<cv::Point> output;
   std::vector<double> distances;

   distances.resize(numPoints * 2);
   double distance;

   int i, j;
   int endPoint = static_cast<int>(contourIn.size());

   for (i = 0; i < contourIn.size() - 1; i++) {
      int cnt = 0;
      int start, end;
      start = i;
      end = i + numPoints;

      // this will never be less then zero, but this code could be used to look on both sides of a contour, so keeping it in, just in case
      // This prevents negative starting position by wrapping around to end of profile
      if (start < 0) {
         start = endPoint + start;
      }

      // This prevents going past end of contour by wrapping around to start of profile
      if (end >= endPoint) {
         end = end - endPoint;
      }

      if (verbose) {
         printf("Start %d End %d\n", start, end);
      }

      // If start is more then end, then need to go through two loops instead of one
      if (start > end) {
         // First loop from start point to the end of the profile
         for (j = start; j < endPoint - 1; j++) {
            if (j != i) {
               distance = sqrt(pow(contourIn[i].x - contourIn[j].x, 2) + pow(contourIn[i].y - contourIn[j].y, 2));
               distances[cnt] = distance;
               cnt++;
            }
         }
         // Start from the first profile point until the end point
         for (j = 0; j <= end; j++) {
            if (j != i) {
               distance = sqrt(pow(contourIn[i].x - contourIn[j].x, 2) + pow(contourIn[i].y - contourIn[j].y, 2));
               distances[cnt] = distance;
               cnt++;
            }
         }
      }
      else {
         // For from start point to the end point
         for (j = start; j <= end; j++) {
            if (j != i) {
               distance = sqrt(pow(contourIn[i].x - contourIn[j].x, 2) + pow(contourIn[i].y - contourIn[j].y, 2));
               distances[cnt] = distance;
               cnt++;
            }
         }
      }

      int match = -1;
      for (j = 0; j < cnt - 1; j++) {
         // Distance is less then previous point?  That could be a bump?!?
         if (distances[j] > distances[j + 1]) {
            double minDist = distances[1];

            if (verbose) {
               printf("Distance Decreased?\n");
            }
            int k;
            for (k = 0; k < cnt; k++) {
               if ((distances[k] < minDist) && (k > 2)) {
                  // Found a match, we have a bump!
                  match = k;
                  if (verbose) {
                     printf("Found match?? %d\n", k);
                  }
               }
               if (verbose) {
                  printf("Distance %d is %lf\n", k, distances[k]);
               }
            }
            break;
         }
      }


      // We have a bump
      if (match != -1) {
         i += match; // skip over this many
         if (i >= endPoint) {
            break;
         }
      }
      else {
         // No bump, so add to the final contour
         output.emplace_back(contourIn[i].x, contourIn[i].y);
      }
   }
   return(output);
}

roverPost::roverPost() {

}

Mat roverPost::processFile(const char* input, int& err, int trackWidth, int wallWidth) {
   err = 0;

   // Use opencv to read image
   Mat inputImage = imread(input, IMREAD_GRAYSCALE);
   Mat outputImage;

   // No image read in?  Exit
   if (inputImage.empty()) {
      err = 1;
      return(outputImage);
   }

   vector<Vec4i> hierarchy;
   std::vector<std::vector<cv::Point>> centerContours;

   Mat thinImage;
   // Generated images may have non 255 values for track, so threshold the image first
   threshold(inputImage, inputImage, 100, 256, 0);

   // Thin the track down to a skeleton
   thinning(inputImage, thinImage);

   // Find the contour of the skeleton using OpenCV findContours
   findContours(thinImage, centerContours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, Point(0, 0));

   // Find the contour with the most points
   int i;
   int maxValue = 0;
   int maxNumberCenter = -1;
   for (i = 0; i < centerContours.size(); i++) {
      if (centerContours[i].size() > maxValue) {
         maxValue = static_cast<int>(centerContours.size());
         maxNumberCenter = i;
      }
   }
   if (maxNumberCenter == -1) {
      err = 2;
      return(outputImage);
   }

   // Working contour is the one with the most points
   std::vector<cv::Point> centerContour = centerContours.at(maxNumberCenter);

   // Remove bumps that are result artifacts from the thinning
   std::vector<cv::Point> centerContour2 = removeBumps(centerContour, 20, false);

   // Create empty image
   outputImage = Mat::zeros(1024, 1024, CV_8UC1);

   // Set default image values to "unknown" value
   outputImage.setTo(cv::Scalar(128));

   // Draw the outer bit of the track.. (the walls)
   for (i = 0; i < centerContour2.size(); i++) {
      Point pt1 = { centerContour2[i].x, centerContour2[i].y };
      Point pt2;
      if (i == centerContour2.size() - 1) {
         pt2 = { centerContour2[0].x, centerContour2[0].y };
      }
      else {
         pt2 = { centerContour2[i + 1].x, centerContour2[i + 1].y };
      }

      line(outputImage, pt1, pt2, Scalar(0), trackWidth + wallWidth, LINE_8);
   }

   // Draw the inner part of the track (surface to drive on)
   for (i = 0; i < centerContour2.size(); i++) {
      Point pt1 = { centerContour2[i].x, centerContour2[i].y };
      Point pt2;
      if (i == centerContour2.size() - 1) {
         pt2 = { centerContour2[0].x, centerContour2[0].y };
      }
      else {
         pt2 = { centerContour2[i + 1].x, centerContour2[i + 1].y };
      }
      line(outputImage, pt1, pt2, Scalar(255), trackWidth, LINE_8);
   }

   return(outputImage);
}

int roverPost::convertAll(int argc, char* argv[], bool verbose) {

   // Need to args to setup conversion
   // argv[2] folder with png files
   // argv[3] output folder with processed png files
   if (argc < 4) {
      printf("Number of args given %d < 4", argc);
      return(-1);
   }

   // For scale of 0.07*31 = Width of 2.17 meters. For full scale 0.7*31 = 21.7 meters
   int trackWidth = 31;
   int wallWidth = 6;

   // Option to change width of a generated track
   if (argc >= 5) {
      trackWidth = atoi(argv[4]);
   }

   // Option to change the width of walls in a generated track.
   if (argc >= 6) {
      wallWidth = atoi(argv[5]);
   }

   printf("Post Processing png files\n");
   printf("Track width %d pixels, wall width %d pixels\n", trackWidth, wallWidth);

   if (trackWidth < 31) {
      fprintf(stderr, "Warning recommend track width of at least 31 pixels but I'm not going to stop you\n");
   }

   if (wallWidth < 4) {
      fprintf(stderr, "Warning recommend wall width of at least 4 pixels but I'm not going to stop you\n");
   }

   int i;

   // Process all files in folder with extension of png
   std::vector<string> files = FindFilesWithExtension(argv[2], ".png");
   string outputFolder = argv[3];

   // Loop through all the png files
   for (i = 0; i < files.size(); i++) {
      char outputName[1025];
      char fileNameNoPath[1025];
      char fileNameNoPath2[1025];
      char fileName[1025];
      removeFileExtension(files[i].c_str(), fileName);
      removePath(files[i].c_str(), fileNameNoPath);
      removeFileExtension(fileNameNoPath, fileNameNoPath2);

      // Output is folder name give by argv[3] and the original png name without the folder and extension (in case want to use something other then png files as input?)
#ifdef _LINUX
      snprintf(outputName, 1024, "%s/%s.png", outputFolder.c_str(), fileNameNoPath2);
#else
      snprintf(outputName, 1024, "%s\\%s.png", outputFolder.c_str(), fileNameNoPath2);
#endif
      printf("Input %s Output %s\n", files[i].c_str(), outputName);

      int err;
      // Process each image
      Mat outputImage = processFile(files[i].c_str(), err, trackWidth, wallWidth);
      if (err == 0) {
         // Write out the processed image of processFile did not fail
         if (!imwrite(outputName, outputImage)) {
            fprintf(stderr,"Unable to write file %s\n", outputName);
         }
      }
   }

   return(0);
}
