#ifndef ROVER_UTILITY_HPP
#define ROVER_UTILITY_HPP

class roverPose;

class roverUtility {
public:
   roverUtility();

   /* For a given wheelbase, steering angle and frictionLimit find the maximum obtainable speed. If pastLimit is true then
    * maxSpeed is beyond the limit */
   double calculateSpeedLimit(double steeringAngleRadians, double maxSpeed, bool& pastLimit, double frictionLimit = 1.24);
   /* For a given wheelbase and maximum turning angle find the minimum turning radius */
   double calculateMinimumSteeringRadius(double maxAngleRadians);
   /* For a given wheelbase and turing angle find the turning radius */
   double calculateTurningRadius(double steerAngleRadians);

  
private:
   double wheelBase;

};

double distanceBetween(double lat1, double long1, double lat2, double long2);

float calculateBeamWidth(float distance, double angleRadians);
double courseTo(double lat1, double long1, double lat2, double long2);

#endif

