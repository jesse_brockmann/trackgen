/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

#ifndef ROVER_MAP_HPP
#define ROVER_MAP_HPP

#include <vector>
#include <opencv2/opencv.hpp>

class roverMap {
public:
   roverMap();
   int load(int argc, char** argv, bool verbose, double scale);
   int generateYAML(const char* mapName, const char* yamlFile, double scale, double offset_x, double offset_y, double heading);
   int findOrigin(const char* name, double scale, double& offset_x, double& offset_y, double& heading);
   int mouseEvent(int x, int y, int event);
   int findCenterLine();
private:
   cv::Point textPt = { 10,20 };
   std::vector<cv::Point> centerLine;
   cv::Mat image;
   cv::Mat image2;
   cv::Point origin;
   cv::Point direction;
   bool haveDirection;
   bool haveOrigin;
   int cnt;
};

#endif
