/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

#include "roverCIRFile.hpp"
#include "roverPost.hpp"
#include "roverMap.hpp"

// Display help!?!
void showHelp() {
   fprintf(stderr, "TrackPreProcess requires at least 3 args\n");
   fprintf(stderr, "Supported options are\n");
   fprintf(stderr, "pre inputDirectory (process cir files)\n");
   fprintf(stderr, "post inputDirectory OutputDirectory [trackWidth] [wallWidth] (process raw images into tracks) []=optional args values in pixels\n");
   fprintf(stderr, "map imageFile1.png imageFile2.png (create map yaml files for full scale)\n");
   fprintf(stderr, "f1tenth imageFile1.png imageFile2.png (create map yaml files for f1tenth scale)\n");
   fprintf(stderr, "TrackPreProcess is by Jesse Brockmann\n");
}

int main(int argc, char* argv[]) {
   // Default args
   bool preProcess = false;
   bool postProcess = false;
   bool verbose = true;
   bool map = false;
   double scale = 0.7;

   if (argc <= 2) {
      showHelp();
      exit(-1);
   }

   if (strcmp(argv[1], "pre") == 0) {
      preProcess = true;
   }
   else if (strcmp(argv[1], "post") == 0) {
      postProcess = true;
   }
   else if (strcmp(argv[1], "map") == 0) {
      map = true;
   }
   else if (strcmp(argv[1], "f1tenth") == 0) {
      map = true;
      scale = 0.07;
   }
   else {
      fprintf(stderr, "Unknown request type of %s\n", argv[1]);
      showHelp();
      exit(-2);
   }

   // Uncomment following line to add a pause to allow attaching to a debugger
   // fgetc(stdin);

   int rv;

   if (map) {
      // Write out a map yaml file for a map
      roverMap mapC;
      rv = mapC.load(argc, argv, verbose, scale);
   }
   else if (postProcess) {
      // Process output profiles into maps
      roverPost post;
      rv = post.convertAll(argc, argv, verbose);
   }
   else if (preProcess) {
      // Convert cir files into png files
      roverCIRFile cirFile;
      rv = cirFile.convertAll(argc, argv, true, verbose);
   }
   return(rv);
}
