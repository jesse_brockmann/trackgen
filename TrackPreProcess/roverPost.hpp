/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

#ifndef ROVER_POST_HPP
#define ROVER_POST_HPP

#include <vector>
#include <opencv2/opencv.hpp>

void thinningIteration(cv::Mat& img, int iter);
void thinning(const cv::Mat& src, cv::Mat& dst);
std::vector<cv::Point> removeBumps(std::vector<cv::Point> contourIn, int numPoints, bool verbose);

class roverPost {
public:
   roverPost();
   int convertAll(int argc, char* argv[], bool verbose);
   cv::Mat processFile(const char* input, int& err, int trackWidth, int wallWidth);
};

#endif
