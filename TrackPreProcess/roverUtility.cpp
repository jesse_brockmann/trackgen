/* Code License
 * This source code was created by Jesse Brockmann (https://www.linkedin.com/in/jesse-brockmann/) in 2023
 * Any use is allowed, however I will accept payment or beers if you find it useful. Also you cannot remove
 * this comment. Some routines are individually licensed and that code will be commented as such in the source.
 * I provide no warranty, promises of security or stability of this code.  It is provided AS-IS!
 */

#define _USE_MATH_DEFINES
#include <cmath>
#include "roverUtility.hpp"
#include "genericUtil.hpp"

roverUtility::roverUtility() {
   wheelBase = 0.;
}

/* This routine is based on Open AI generated code */
double roverUtility::calculateMinimumSteeringRadius(double maxAngleRadians) {
   return(wheelBase / sin(maxAngleRadians));
}

/* This routine is based on Open AI generated code */
double roverUtility::calculateTurningRadius(double steerAngleRadians) {
   if (steerAngleRadians == 0.) {
      return(99999);
   }

   // Calculate turning radius
   return wheelBase / tan(steerAngleRadians);
}

double roverUtility::calculateSpeedLimit(double steeringAngleRadians, double maxSpeed, bool& pastLimit, double frictionLimit) {
   pastLimit = false;
   if (fabs(steeringAngleRadians) < 0.03) {
      return(maxSpeed);
   }

   double turningRadius = calculateTurningRadius(steeringAngleRadians);
   double maxV = sqrt(turningRadius * GRAVITY * frictionLimit);
   if (maxSpeed > maxV) {
      pastLimit = true;
   }
   else {
      maxV = maxSpeed;
   }
   return(maxV);
}

/* This routine is based on Open AI generated code */
double distanceBetween(double lat1, double lon1, double lat2, double lon2) {
   const double R = 6372795; // Earth's radius in meters

   // Convert latitudes and longitudes from degrees to radians
   lat1 = genericUtil::toRadians(lat1);
   lat2 = genericUtil::toRadians(lat2);
   lon1 = genericUtil::toRadians(lon1);
   lon2 = genericUtil::toRadians(lon2);

   // Calculate differences
   double dLat = lat2 - lat1;
   double dLon = lon2 - lon1;

   // Apply Haversine formula
   double a = std::sin(dLat / 2) * std::sin(dLat / 2) +
      std::cos(lat1) * std::cos(lat2) *
      std::sin(dLon / 2) * std::sin(dLon / 2);
   double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));
   double distance = R * c;

   return(distance); // Distance in meters
}

/* This routine is based on Open AI generated code */
double courseTo(double lat1, double lon1, double lat2, double lon2) {
   // Convert latitudes and longitudes from degrees to radians
   lat1 = genericUtil::toRadians(lat1);
   lat2 = genericUtil::toRadians(lat2);
   lon1 = genericUtil::toRadians(lon1);
   lon2 = genericUtil::toRadians(lon2);

   // Calculate differences
   double dLon = lon2 - lon1;

   // Calculate course
   double y = std::sin(dLon) * std::cos(lat2);
   double x = std::cos(lat1) * std::sin(lat2) - std::sin(lat1) * std::cos(lat2) * std::cos(dLon);
   double initialBearing = std::atan2(y, x);

   // Normalize the radians
   double course = genericUtil::wrapRadians2PI(initialBearing);
   return(course);
}

/* This routine is based on Open AI generated code */
// Function to calculate the width of the LiDAR beam
float calculateBeamWidth(float distance, double angleRadians) {
   // Calculate the width of the beam
   // The width is twice the length of the base of the right-angled triangle formed
   // by cutting the isosceles triangle in half
   float width = 2.0f * distance * tanf(static_cast<float>(angleRadians) / 2.0f);
   return width;
}

