import os
import sys
from PIL import Image
import matplotlib.pyplot as plt

outputFolder1="outNN"
outputFolder2="out"
numSeeds=100

if len(sys.argv) == 2:
   numSeeds=int(sys.argv[1])

seeds=f'1-{numSeeds}'
print(f'Will generate {numSeeds} images')

#If folder does not exist, create it
if not os.path.exists(outputFolder1):
    print(f'Creating foler {outputFolder1}')
    os.makedirs(outputFolder1)

print(f'Writing images to folder {outputFolder1}')
os.system(f'python stylegan3/gen_images.py --outdir={outputFolder1} --network=PreTrainedNetwork/network-snapshot-000315.pkl --noise-mode=random --seeds={seeds}')

print(f'Generating map png files in folder {outputFolder2}')

#If folder does not exist, create it
if not os.path.exists(outputFolder2):
    print(f'Creating foler {outputFolder2}')
    os.makedirs(outputFolder2)

print(f'Now processing the png files')

if sys.platform == 'linux':
   os.system(f'TrackPreProcess/TrackPreProcess post {outputFolder1} {outputFolder2}')
   os.chdir('out')
   os.system('eog seed0001.png')
else:
   os.system(f'TrackPreProcess\\bin\\TrackPreProcess.exe post {outputFolder1} {outputFolder2}')
