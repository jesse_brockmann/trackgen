# Line numbers to replace (1-indexed)
line_numbers_to_replace = {136: "        img = (img.permute(0, 1, 2, 3) * 127.5 + 128).clamp(0, 255).to(torch.uint8)\n", 137: "        PIL.Image.fromarray(img[0].cpu().numpy()[0]).save(f'{outdir}/seed{seed:04d}.png')\n"}

# File path
file_path = 'stylegan3/gen_images.py'

# Read the file
with open(file_path, 'r') as file:
    lines = file.readlines()

# Modify specific lines
for line_number in line_numbers_to_replace:
    # Adjust for 0-indexing
    if 0 <= line_number - 1 < len(lines):
        lines[line_number - 1] = line_numbers_to_replace[line_number]

# Write the modified content back to the file
with open(file_path, 'w') as file:
    file.writelines(lines)